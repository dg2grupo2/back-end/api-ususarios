package br.com.desafio.app.apiusuarios.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.desafio.app.apiusuarios.dto.UsuarioDTO;
import br.com.desafio.app.apiusuarios.dto.UsuarioResDTO;
import br.com.desafio.app.apiusuarios.models.Usuario;
import br.com.desafio.app.apiusuarios.repositories.UsuarioRepository;
import br.com.desafio.app.apiusuarios.utils.UsuarioStatusEnum;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository repository;

    public List<Usuario> getUsuarios() {
        List<Usuario> lista = repository.findAll();
        return lista;
    }

    public UsuarioDTO getUsuarioByName(String nome) {
        try {
            Usuario usuario = repository.findByNome(nome);
            if ( usuario == null ) {
                return null;
            }
            return new UsuarioDTO(usuario);
        } catch (Exception e) {
            e.printStackTrace(); 
            return null;              
        }
    }

    public UsuarioDTO getUsuarioById(Long id) {
        try {
            Usuario usuario = repository.findById(id).get();
            if ( usuario == null ) {
                return null;
            }
            return new UsuarioDTO(usuario);
        } catch (Exception e) {
            e.printStackTrace(); 
            return null;              
        }
    }

    public UsuarioResDTO cadastrarUsuario (UsuarioDTO usuario) {        
        try {
            Usuario user = new Usuario(usuario);
            user = repository.saveAndFlush(user);
            if ( user == null ) {
                return null;
            }
            UsuarioResDTO usuarioSalvo = new UsuarioResDTO(user);
            return usuarioSalvo;            
            
        } catch (Exception e) {
            e.printStackTrace(); 
            return null;  
        }

    }

    public String alterarUsuario(UsuarioDTO usuarioBody) {

        try {
            Usuario usuarioBanco = repository.findById(usuarioBody.getId()).orElse(null);
            if ( usuarioBanco == null ) {
                return "Usuário não encontrado";
            }
            if ( usuarioBody.getNome() != null ) {
                usuarioBanco.setNome(usuarioBody.getNome());                
            }
            if ( usuarioBody.getCpf() != null ) {
                usuarioBanco.setCpf(usuarioBody.getCpf());                
            }
            if ( usuarioBody.getTelefone() != null ) {
                usuarioBanco.setTelefone(usuarioBody.getTelefone());                
            }
            if ( usuarioBody.getDataNascimento() != null ) {
                usuarioBanco.setDataNascimento(usuarioBody.getDataNascimento());                
            }
            if ( usuarioBody.getEmail() != null ) {
                usuarioBanco.setEmail(usuarioBody.getEmail());                
            }
            if ( usuarioBody.getStatus() != null ) {
                usuarioBanco.setStatus(usuarioBody.getStatus().toString());                
            }
            usuarioBanco = repository.save(usuarioBanco);
            if ( usuarioBanco == null ) {
                return "Erro ao salvar";
            }
            return "Dados atualizados com sucesso";
        } catch (Exception e) {
            e.printStackTrace(); 
            return null;
        }
    }

    public String editarStatusUsuario(UsuarioDTO usuario) {
        try {
            Usuario usuarioEditado = repository.findById(usuario.getId()).orElse(null);

            if (usuarioEditado == null) {
                return null;
            }
            if (usuario.getStatus().getStatus() < 1 || usuario.getStatus().getStatus() > UsuarioStatusEnum.values().length) {
                return "Status inválido";
            }
            if ( usuarioEditado.getStatus() != usuario.getStatus().getStatus().toString() ) {
                usuarioEditado.setStatus(UsuarioStatusEnum.getKey(usuario.getStatus().getStatus()).get().toString());
                System.out.println("*****************" + usuarioEditado.getStatus());
                repository.save(usuarioEditado);
                return "Status do Usuário Atualizado";
            }
            return null;
        } 
        catch (Exception e) {
            e.printStackTrace(); 
            return null;
        }
    }

    public List<UsuarioResDTO> findByNomeContaining(String nome) {        
        List<Usuario> lista = repository.findByNomeContaining(nome);
        List<UsuarioResDTO> listaDTO = lista.stream().map(usuario -> new UsuarioResDTO(usuario)).collect(Collectors.toList());
        return listaDTO;
    }
}
