package br.com.desafio.app.apiusuarios.utils;

import java.util.Arrays;
import java.util.Optional;

public enum UsuarioStatusEnum {
    ativo(1),
    inativo(2);


    private Integer status;
    UsuarioStatusEnum(Integer status) {
        this.status = status;
    }


    public Integer getStatus() {
        return status;
    }   


    public static Optional<UsuarioStatusEnum> getKey(Integer status) {
        return Arrays.stream(UsuarioStatusEnum.values())
                                              .filter(valor -> valor.status == status)
                                              .findFirst();
    }

    public static UsuarioStatusEnum valueOf(Integer status) throws Exception {
        for (UsuarioStatusEnum valor : UsuarioStatusEnum.values()) {
            if ( valor.getStatus() == status ) {
                System.out.println(valor);
                return valor;
            }
        }
        throw new Exception("Status Inválido");
    }
}
