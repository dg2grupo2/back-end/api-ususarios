package br.com.desafio.app.apiusuarios.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.desafio.app.apiusuarios.dto.UsuarioDTO;
import br.com.desafio.app.apiusuarios.dto.UsuarioResDTO;
import br.com.desafio.app.apiusuarios.models.Usuario;
import br.com.desafio.app.apiusuarios.services.UsuarioService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@CrossOrigin("*")
@RestController
@RequestMapping("/usuarios")
@Api(value = "Usuários")
public class UsuariosController {

    @Autowired
    private UsuarioService service;

    @ApiOperation(value = "Retorna Lista de Usuários")
    @GetMapping
    public List<Usuario> getUsuarios() {
        List<Usuario> usuarios = service.getUsuarios();
        return usuarios;
    }

    @ApiOperation(value = "Realiza Busca de Usuários pelo Nome")
    @GetMapping("/busca")
    public ResponseEntity<List<UsuarioResDTO>> getUsuarioByNome(@RequestParam String nome) {
        System.out.println("Antes de lista");
        List<UsuarioResDTO> usuarioDTO = service.findByNomeContaining(nome);
        if ( usuarioDTO == null ) {
            return ResponseEntity.status(404).body(null);
        } 
        return ResponseEntity.ok(usuarioDTO);
    }

    @ApiOperation(value = "Realiza Busca de Usuários pelo Id")
    @GetMapping("/{id}")
    public ResponseEntity<?> getUsuarioById(@PathVariable("id") Long id) {
        UsuarioDTO usuarioDTO = service.getUsuarioById(id);
        if ( usuarioDTO == null ) {
            return ResponseEntity.status(404).body(null);
        } 
        return ResponseEntity.ok(usuarioDTO);
    }    

    @ApiOperation(value = "Realiza Cadastro de Usuários")
    @PostMapping
    public ResponseEntity<?> cadastrarUsuario(@RequestBody UsuarioDTO usuario) {
        UsuarioResDTO  usuarioDTO= service.cadastrarUsuario(usuario);
        if ( usuarioDTO == null ) {
            return ResponseEntity.status(404).body(null);
        } 
        return ResponseEntity.ok(usuarioDTO);
    }
    
    @ApiOperation(value = "Realiza Alterações no Cadastro de Usuários")
    @PutMapping
    public ResponseEntity<?> alterarUsuario(@RequestBody UsuarioDTO usuarioBody) {
        String resposta = service.alterarUsuario(usuarioBody);
        if ( resposta == null ) {
            return ResponseEntity.status(404).body("Erro");
        }
        return ResponseEntity.ok(resposta);
    }

    @ApiOperation(value = "Realiza Alterações no Status do Cadastro de Usuários")
    @PutMapping("/alterar-status")
    public ResponseEntity<?> editarStatusUsuario(@RequestBody UsuarioDTO usuario) {
        String resposta = service.editarStatusUsuario(usuario);
        if ( resposta == null ) {
        return ResponseEntity.status(404).body("Erro");
        }
        return ResponseEntity.ok(resposta);
    }

        
}

