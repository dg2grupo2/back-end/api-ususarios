package br.com.desafio.app.apiusuarios.dto;

import java.time.LocalDate;

import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.br.CPF;

import br.com.desafio.app.apiusuarios.models.Usuario;
import br.com.desafio.app.apiusuarios.utils.UsuarioStatusEnum;

public class UsuarioDTO {
    @Id
    @NotEmpty
    private Long id;
    @NotEmpty
    private String nome;
    @CPF
    @NotEmpty
    private String cpf;
    @NotEmpty
    private String telefone;
    @NotEmpty
    private LocalDate dataNascimento;
    @Email
    @NotEmpty
    private String email;
    @NotEmpty
    private UsuarioStatusEnum status;    
    

    public UsuarioDTO(@NotEmpty Long id, @NotEmpty String nome, @CPF @NotEmpty String cpf, @NotEmpty String telefone,
            @NotEmpty LocalDate dataNascimento, @Email @NotEmpty String email, @NotEmpty UsuarioStatusEnum status) {
        this.id = id;
        this.nome = nome;
        this.cpf = cpf;
        this.telefone = telefone;
        this.dataNascimento = dataNascimento;
        this.email = email;
        this.status = status;
    }
    public UsuarioDTO(Usuario usuario) {
        this.id = usuario.getId();
        this.nome = usuario.getNome();
        this.cpf = usuario.getCpf();
        this.telefone = usuario.getTelefone();
        this.dataNascimento = usuario.getDataNascimento();
        this.email = usuario.getEmail();
    }
    public UsuarioDTO() {
    }


    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public String getCpf() {
        return cpf;
    }
    public void setCpf(String cpf) {
        this.cpf = cpf;
    }        
    public String getTelefone() {
        return telefone;
    }
    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
    public LocalDate getDataNascimento() {
        return dataNascimento;
    }
    public void setDataNascimento(LocalDate dataNascimento) {
        this.dataNascimento = dataNascimento;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public UsuarioStatusEnum getStatus() {
        return status;
    }
    public void setStatus(UsuarioStatusEnum status) {
        this.status = status;
    }    
    
}

