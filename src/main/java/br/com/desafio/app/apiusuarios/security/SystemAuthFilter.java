package br.com.desafio.app.apiusuarios.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

public class SystemAuthFilter extends OncePerRequestFilter {
    
    AdministradorClient administradorClient = new AdministradorClient();
    
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        
                if ( request.getServletPath().contains("/swagger") || 
                     request.getServletPath().contains("/webjar") ||
                     request.getServletPath().contains("/v2/api-docs") ) {

                        SecurityContextHolder.getContext().setAuthentication(administradorClient.isAuthenticated());
                        System.out.println(request.getServletPath());
                        filterChain.doFilter(request, response);
                    return;
                }

                System.out.println(request.getServletPath());
                Authentication auth = administradorClient.isAuthorized(request.getHeader("Authorization"));
                SecurityContextHolder.getContext().setAuthentication(auth);

        filterChain.doFilter(request, response);
        
    }
    
}
