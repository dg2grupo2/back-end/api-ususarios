CREATE TABLE usuario (
  id BIGINT NOT NULL AUTO_INCREMENT,
  nome VARCHAR(100) NOT NULL,
  cpf VARCHAR(15) NOT NULL UNIQUE,
  telefone VARCHAR(20) DEFAULT NULL,
  email VARCHAR(100) NOT NULL UNIQUE,
  data_nascimento DATE DEFAULT NULL,
  status VARCHAR(40) NOT NULL,
  PRIMARY KEY (id)
);

INSERT INTO usuario ( nome, cpf, telefone, email, data_nascimento, status)
VALUES ( 'Sandro Araujo Machado', '13254678545', '2136874568', 'sandroam@hotmail.com', '1976-01-24', 'ativo'),
( 'Angelica Pereira Silva', '24565845745', '1136668754', 'angelicaps@hotmail.com', '1966-02-14', 'inativo'),
( 'Matheus Luiza Alves', '87985566774', '5136101358', 'matheusla@hotmail.com', '1946-08-11', 'ativo'),
( 'Ana Figueira Cruz', '88966477747', '4836559851', 'anafc@hotmail.com', '1977-11-20', 'ativo');