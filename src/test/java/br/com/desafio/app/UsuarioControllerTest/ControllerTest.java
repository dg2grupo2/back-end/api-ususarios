package br.com.desafio.app.UsuarioControllerTest;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import br.com.desafio.app.apiusuarios.models.Usuario;
import br.com.desafio.app.apiusuarios.repositories.UsuarioRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class ControllerTest {
           
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TestRestTemplate restTemplate;
    
    @MockBean
    private UsuarioRepository repo;

    private HttpEntity<Void> protectedHeader;
    private HttpEntity<Void> adminHeader;
    private HttpEntity<Void> wrongHeader;

    @Before
    public void configProtectedHeaders() {
        String str = "{\"email\": \"email@email.com.br\", \"senha\": \"teste123\"}";
        HttpHeaders headers = restTemplate.postForEntity("/login", str, String.class).getHeaders();
        this.protectedHeader = new HttpEntity<>(headers);
    }

    @Before
    public void configAdminHeaders() {
        String str = "{\"email\": \"email@email.com.br\", \"senha\": \"teste123\"}";
        HttpHeaders headers = restTemplate.postForEntity("/login", str, String.class).getHeaders();
        this.adminHeader = new HttpEntity<>(headers);
    }

    @Before
    public void configWrongHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "111111");
        this.wrongHeader = new HttpEntity<>(headers);
    }
             
    @Test
    public void getUsuariosTest() {
        List<Usuario> usuarios = Arrays.asList(new Usuario( 
        1L,
        "Caru", 
        "123456789",
        "123456789",
        LocalDate.of(1212, 12, 12),
        "email@email.com",
        "ativo" ),
        new Usuario( 2L,
        "Gui", 
        "987654321",
        "987654321",
        LocalDate.of(1212, 12, 12),
        "email@email.com",
        "ativo" ));
        
        BDDMockito.when(repo.findAll()).thenReturn((List<Usuario>) usuarios);
        
        ResponseEntity<String> response = restTemplate.exchange("/usuarios", HttpMethod.GET, protectedHeader, String.class);
        assertThat(response.getStatusCodeValue()).isEqualTo(200);
    }
}
