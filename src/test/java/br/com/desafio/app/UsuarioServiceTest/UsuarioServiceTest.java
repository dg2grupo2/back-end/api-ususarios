package br.com.desafio.app.UsuarioServiceTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;

import br.com.desafio.app.apiusuarios.models.Usuario;
import br.com.desafio.app.apiusuarios.repositories.UsuarioRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UsuarioServiceTest {

    @Autowired
    private UsuarioRepository repo;

    @Test
    public void cadastrarUsuarioTest() {

        Usuario usuario = new Usuario( 1L,
                                       "Caru", 
                                       "123456789",
                                       "123456789",
                                       LocalDate.of(1212, 12, 12),
                                       "email@email.com",
                                       "ativo" );

        repo.save(usuario);

        assertThat(usuario.getId()).isNotNull();
        assertThat(usuario.getNome()).isEqualTo("Caru");
        assertThat(usuario.getCpf()).isEqualTo("123456789");
        assertThat(usuario.getTelefone()).isEqualTo("123456789");
        assertThat(usuario.getDataNascimento()).isEqualTo(LocalDate.of(1212, 12, 12));
        assertThat(usuario.getEmail()).isEqualTo("email@email.com");
        assertThat(usuario.getStatus()).isEqualTo("ativo");

    }

    @Test
    public void getUsuarioByNameTest() {

        Usuario usuario = new Usuario( 1L,
                                       "Caru", 
                                       "123456789",
                                       "123456789",
                                       LocalDate.of(1212, 12, 12),
                                       "email@email.com",
                                       "ativo" );

        repo.save(usuario);
        var usuarioRetornado = repo.findByNome(usuario.getNome());

        assertThat(usuarioRetornado.getNome()).isEqualTo("Caru");

    }

    @Test
    public void getUsuarioByIdTest() {

        Usuario usuario = new Usuario( 1L,
                                       "Caru", 
                                       "123456789",
                                       "123456789",
                                       LocalDate.of(1212, 12, 12),
                                       "email@email.com",
                                       "ativo" );

        repo.save(usuario);
        var usuarioRetornado = repo.findById(usuario.getId());

        assertThat(usuarioRetornado.get().getId()).isNotNull();

    }

    @Test
    public void alterarUsuarioTest() {

        Usuario usuario = new Usuario( 1L,
                                       "Caru", 
                                       "123456789",
                                       "123456789",
                                       LocalDate.of(1212, 12, 12),
                                       "email@email.com",
                                       "ativo" );

        repo.save(usuario);        
        usuario.setNome("Carolina");
        repo.save(usuario);

        assertThat(usuario.getNome()).isEqualTo("Carolina");
    }

    @Test
    public void editarStatusUsuarioTest() {

        Usuario usuario = new Usuario( 1L,
                                       "Caru", 
                                       "123456789",
                                       "123456789",
                                       LocalDate.of(1212, 12, 12),
                                       "email@email.com",
                                       "ativo" );

        repo.save(usuario);        
        usuario.setStatus("inativo");
        repo.save(usuario);

        assertThat(usuario.getStatus()).isEqualTo("inativo");
    }
    
}
